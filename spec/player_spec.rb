require '../lib/player'

players = [Player.new("Player 1"),Player.new("Player 2")]
puts "#{Player.new("Player 1")} should be an instance of Player"
puts "#{players[0].name} should be 'Player 1'"
puts "#{players[0].points} should be 0"
puts "#{players[0].add_points(50)} should be 50"
