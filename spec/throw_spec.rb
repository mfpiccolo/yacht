require '../lib/die'
require '../lib/throw'


throw1 = Throw.new(Die.new)
puts "#{throw1.dice} should be an empty array."
throw1.roll
puts "#{throw1.dice} should be an array of five random numbers between 1 and 6."
puts "#{throw1.roll} should return an array of five dice."
throw1 = Throw.new(Die.new)
throw1.set_dice([1,2,3,4,6])
puts "#{throw1.dice} should be [1,2,3,4,6]"
puts "#{throw1.reroll('1,2,6')} should be [1,2,6] and two random numbers."
throw1.set_dice([1,2,3,3,3])
puts "#{throw1.valid_choice?('4,4,3')} should return false"
puts "#{throw1.valid_choice?('3,3,3,3')} should return false"
puts "#{throw1.valid_choice?('3,3,3')} should be true"
puts "#{throw1.valid_choice?('2,1')} should be true"
puts "#{throw1.valid_choice?('1,2,3,3,3,4')} should be false"
puts "#{throw1.valid_choice?('1,2,3,3,3,4,6')} should be false"
