require '../lib/player'
require '../lib/turn'


players = [Player.new("Player 1"),Player.new("Player 2")]
turn = Turn.new(players[0])
puts "#{players[0].points} should 0"
turn.roll([1,1,1,1,1])
puts "#{turn.hand} should return 'Five of a kind."
puts "#{turn.score} should return 50"
turn.roll([1,1,1,1,4])
puts "#{turn.hand} should return 'Four of a kind.'"
puts "#{turn.score} should return '8'"
turn.roll([1,1,1,3,3])
puts "#{turn.hand} should return 'Full house."
puts "#{turn.score} should be 25"
turn.roll([1,2,3,1,1])
puts "#{turn.hand} should return 'Three of a kind."
puts "#{turn.score} should be 3"
turn.roll([1,2,3,4,5])
puts "#{turn.hand} should return 'Straight'."
puts "#{turn.score} should be 40"
turn.roll([1,4,4,5,6])
puts "#{turn.hand} should be 'You got nothing!'"
puts "#{turn.score} should be 0"
puts "#{players[0].points} should be 123 something"
turn.roll([1,3,2,6,4])
puts "#{turn.hand} should be 'Small straight'"
puts "#{turn.score} should be 30"