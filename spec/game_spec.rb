require '../lib/player'
require '../lib/game'

players = [Player.new("Player 1"),Player.new("Player 2")]
game = Game.new(players)
puts "#{game} is an instance of Game"
puts "#{game.rounds} should equal 0"
game.add_round!
puts "#{game.rounds} should equal 1"
puts "#{game.over?} should be false."
12.times { game.add_round! }
puts "#{game.over?} should be true."
#player[0].add_points(100)