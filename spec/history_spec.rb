#history.save(array)
#history.exists?(array)
require '../lib/history'

history = History.new
puts "#{history} is an instance of history."

history.save([3,3,4,4,4])
history.save([1,4,3,2,5])
history.save([3,3,3,1,2])
puts "#{history.bank} should return [[3,3,4,4,4],[1,2,3,4,5],[3,3,3]]"
puts "#{history.valid?([3,6,3,2,3])} should be false."
puts "#{history.valid?([2,2,2,1,1])} should be true."
history.save([2,2,2,1,1])
puts "#{history.valid?([2,2,2,1,1])} should be false."
