require '../lib/player'
require '../lib/turn'


players = [Player.new("Player 1"),Player.new("Player 2")]
turn = Turn.new(players[0])
puts "#{turn.roll([1,1,1,1,1])} should return 'Five of a kind."
puts "#{turn.score} should return 50"
puts "#{turn.roll([1,1,1,1,4])} should return 'Four of a kind.'"
puts "#{turn.score} should return '8'"
puts "#{turn.roll([1,1,1,3,3])} should return 'Full house."
puts "#{turn.score} should be 25"
puts "#{turn.roll([1,2,3,4,5])} should return 'Straight'."
puts "#{turn.score} should be 40"
puts "#{turn.roll([1,4,4,5,6])} should be 'You got nothing!'"
