require './lib/player'
require './lib/turn'
require './lib/throw'
require './lib/game'
require './lib/die'
require './lib/history'

puts "Welcome to Yacht dice game!"
players = [Player.new("Player 1"), Player.new("Player 2")]
game = Game.new(players)
dice = Throw.new(Die.new) 
history = History.new

until game.over?
	players.each do |player|
		puts "#{player.name}, it is your turn to roll."
		turn = Turn.new(player)
		turn.roll(dice.roll) #Turn should roll the dice, have initialize call roll
		puts "You rolled: #{dice.dice}."
    2.times do |x|
      input = false
      until input == true
        puts "Please enter in the die you would like to choose seperated by commas. example: for [5,5,3,5,2] you would type in 5,5,5 to save your fives and roll two new dice."
        string = String.new(gets.chomp)
        input = dice.valid_choice?(string)
        puts "Invalid input." unless input == true
      end
      dice.reroll(string)
      turn.roll(dice.dice)
      puts "Your new hand is #{dice.dice}"
    end
      if (history.valid?(dice.dice))
        history.save(dice.dice)
        puts "#{player.name} has rolled: #{turn.hand}."
      else
        player.points -= turn.score
        puts "Somebody already rolled your hand!"
        puts "#{player.name} scores zero!"
      end
      puts "#{player.name} has a total current score of #{player.points}."
	end
	game.add_round!
end

puts "Congratulations, #{game.winner.name}, you have won the game!"
