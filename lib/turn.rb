class Turn
	attr_reader :player, :hand, :score #Check if necessary, if not, delete, otherwise, unit test
	
  def initialize(player)
		@player = player
	end
  
	
  def roll(dice_array)
		@array = dice_array

    if five?
			@score = 50
			@hand = "Five of a kind."
		elsif four?
				@score = @array.select{|element| @array.count(element) > 2 }.inject(:+)
				@hand = "Four of a kind."
		elsif full_house? 
				@score = 25
				@hand = "Full house."
    elsif three?
      @score = @array.select{|element| @array.count(element) > 2 }.inject(:+)
      @hand = "Three of a kind."
		elsif straight?
			@score = 40
			@hand = "Straight"
    elsif small?
      @score = 30
      @hand = "Small straight"
		else
			@score = 0
			@hand = "You got nothing!"
		end	
    @player.add_points(@score)
	end

  private   

  def die_total(array)
    array.inject {|total ,die_number| total += die_number}
  end
  
  def five?
    @array.select{|element| @array.count(element) > 4 }.length > 4    
  end
  
  def four?
     @array.select{|element| @array.count(element) > 3 }.length > 3
  end
  
  def full_house?
    @array.uniq.map! {|element| @array.count(element) }.sort!.reverse!-[1] == [3,2]
  end

  def three?
    @array.select{|element| @array.count(element) > 2 }.length > 2
  end

  def straight?
    max_straight == 5
  end

  def small?
    max_straight == 4
  end

  def max_straight
    sorted = @array.sort.uniq
    max_find = 0
    sum = 1
    (1...sorted.length).each do |i|    
      if sorted[i]-sorted[i-1] == 1 
        sum += 1
      else
        sum = 0
      end
      max_find = sum if sum > max_find
    end
    max_find
  end
end