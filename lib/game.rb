class Game
	attr_reader :rounds

	def initialize(players)
		@players = players
		@rounds = 0	
	end
	
	def over?
	 	@rounds == 13
	end
	
	def add_round!
		@rounds += 1
	end	
	
	def winner
		@players.max_by { |player| player.points }
	end
end
