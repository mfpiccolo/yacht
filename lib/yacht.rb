class Player
	attr_reader :name

	def initialize(name)

		@name = name
	end
end

class Game
	attr_reader :players, :ticker
	def initialize(players)
		@players = players
		@ticker = 0	
	end
	def over?
	 	@ticker == 13 ? true : false
	end
	def add_tick!
		@ticker += 1
	end	


end

class Dice
	attr_reader :dice
	def initialize
		@dice = []
	end
	def roll
		@dice = []
		5.times { |die| @dice << rand(1..6)}
	end
end

class Turn
	attr_reader :player
	def initialize(player)
		@player = player
	end
	def hand
	end
end
