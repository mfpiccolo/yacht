
class Throw
	attr_reader :dice

	def initialize(die)
		@die = die
    @dice = []
	end

	def roll
		@dice = []
		5.times { |die| @dice << @die.roll!}
		@dice
	end

	def set_dice(array)
		@dice = array
	end

	def reroll(string)
		hold_dice = make_array(string)
		(@dice.length-hold_dice.length).times do |i|
			hold_dice << @die.roll!
		end
		@dice = hold_dice
	end

	def valid_choice?(string)
		new_array = make_array(string)
		(new_array-@dice).size == 0 && new_array.uniq.all? { |e| new_array.count(e) <= @dice.count(e) }				
	end

	private

	def make_array(string)
		string.split(',').map { |item| item.to_i }
	end
end