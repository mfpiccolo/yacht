class History
  attr_reader :bank

  def initialize
    @bank = []
    @array = []
  end

  def valid?(array)
    @array = array
    if five?
      @array = @array.select{|element| @array.count(element) > 4 }    
    elsif four?
      @array = @array.select{|element| @array.count(element) > 3 } 
    elsif full_house? 
      @array = @array.sort!
    elsif three?
      @array = @array.select{|element| @array.count(element) > 2 }
    elsif straight?
      @array = @array.sort!
    else 
      return false
    end
    bank.include?(@array) ? false : true
  end

  def save(array)
    @array = array
    if valid?(@array) == true
      bank << @array
    end
  end

  private   

  def five?
    @array.select{|element| @array.count(element) > 4 }.length > 4    
  end
  
  def four?
     @array.select{|element| @array.count(element) > 3 }.length > 3
  end
  
  def full_house?
    @array.uniq.map! {|element| @array.count(element) }.sort!.reverse!-[1] == [3,2]
  end

  def three?
    @array.select{|element| @array.count(element) > 2 }.length > 2
  end

  def straight?
    max_straight == 5
  end

  def small?
    max_straight == 4
  end

  def max_straight
    sorted = @array.sort.uniq
    max_find = 0
    sum = 1
    (1...sorted.length).each do |i|    
      if sorted[i]-sorted[i-1] == 1 
        sum += 1
      else
        sum = 0
      end
      max_find = sum if sum > max_find
    end
    max_find
  end

end