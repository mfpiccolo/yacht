class Die
	attr_reader :dice
	def initialize
		@dice = []
	end
	def roll
		@dice = []
		5.times { |die| @dice << rand(1..6)}
		@dice
	end
end