## yacht


### A simple dice game.

Contains the following classes:

* #### player

    Keeps track of player points and player name.

* #### turn
  
    Tracks five dice rolls.

* #### throw
  
    Throws five dice.

* #### game

    End after 13 turns and decides the winner.

* #### die

    Rolls a single die.
